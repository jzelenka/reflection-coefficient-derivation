# Reflection coefficient - derivation

Reflection coefficient for connection of two circuit parts with different impedance

### Final formula
```math
\Gamma = \dfrac{-Z_{in} + Z_{out}}{Z_{in} + Z_{out}} = \dfrac{Z_{out} - Z_{in}}{Z_{out} + Z_{in}}\qquad(21)
```

### Notation

```math
U = \textit{Voltage (Volt, V)} \\
Z = \textit{Impedance (Ohm, }\Omega \textit{)} \\
S = \textit{Power (Watt, W)} \\
\Gamma = \textit{reflection coefficient (arb.)}\\
```

### Premises

Reflection coefficient is a ratio between arriving and reflected wave.
```math
\Gamma = \dfrac{U_{refl}}{U_{in}}\qquad(1)
```

Voltage on the one side of the connection is equal to the voltage on the other side of the connection
```math
U_{in} + U_{refl} = U_{out}\qquad(2)
```

The power delivered to the nod is equal to the power withdrawn.
```math
S_{in} = S_{refl} + S_{out}\qquad(3)
```

Power is equal to the quadrate of voltage divided by the impedance.
```math
S = \dfrac{U^2}{Z}\qquad(4)
```

Quadratic equation.
```math
x = \dfrac{b \pm \sqrt{b^2-4ac}}{2a}\qquad(5)
```

## Derivation

We start into putting equatin (4) into (3)
```math
\dfrac{U_{in}^2}{Z_{in}} = \dfrac{U_{out}^2}{Z_{out}} + \dfrac{U_{refl}^2}{Z_{in}} \qquad(6)
```

We can substritute $`U_{out}`$ by using equation (2)
```math
\dfrac{U_{in}^2}{Z_{in}} = \dfrac{(U_{refl}+U_{in})^2}{Z_{out}} + \dfrac{U_{refl}^2}{Z_{in}} \qquad(7)
```

Next we substitute $`U_{in}`$ by equation (1)
```math
\dfrac{(\dfrac{U_{refl}}{\Gamma})^2}{Z_{in}} = \dfrac{(U_{refl}+(\dfrac{U_{refl}}{\Gamma}))^2}{Z_{out}} + \dfrac{U_{refl}^2}{Z_{in}} \qquad(8)
```

Now we can put $`U_{refl}`$ before parentheses in the first and the second fraction
```math
\dfrac{U_{refl}^2(\dfrac{1}{\Gamma})^2}{Z_{in}} = \dfrac{U_{refl}^2(1+(\dfrac{1}{\Gamma}))^2}{Z_{out}} + \dfrac{U_{refl}^2}{Z_{in}} \qquad(9)
```

And divide the whole equation by $`U_{refl}^2`$.
```math
\dfrac{(\dfrac{1}{\Gamma})^2}{Z_{in}} = \dfrac{(1+(\dfrac{1}{\Gamma}))^2}{Z_{out}} + \dfrac{1}{Z_{in}} \qquad(10)
```

Lets reduce the number of brackets.
```math
\dfrac{\dfrac{1}{\Gamma^2}}{Z_{in}} = \dfrac{1+2\dfrac{1}{\Gamma} +\dfrac{1}{\Gamma^2}}{Z_{out}} + \dfrac{1}{Z_{in}} \qquad(11)
```

And multiply the whole equation by $`\Gamma^2`$.
```math
\dfrac{1}{Z_{in}} = \dfrac{\Gamma^2+2\Gamma + 1}{Z_{out}} + \dfrac{\Gamma^2}{Z_{in}} \qquad(12)
```

Another multiplication, now by $`{Z_{in}}{Z_{out}}`$ and we arrive into fraction-free equation.
```math
Z_{out} = Z_{in} \Gamma^2  + 2 Z_{in}\Gamma + Z_{in} + Z_{out} \Gamma^2 \qquad(13)
```

Lets unify elements by  $`\Gamma`$.
```math
0 = (Z_{in} + Z_{out}) \Gamma^2  + 2 Z_{in}\Gamma + Z_{in} - Z_{out} \qquad(14)
```

And solve the quadratic equation.
```math
\Gamma = \dfrac{-2 Z_{in} \pm \sqrt{(2 Z_{in})^2 - 4{(Z_{in} + Z_{out})(Z_{in} - Z_{out})}}}{2(Z_{in} + Z_{out})}\qquad(15)
```

Get rid of parentheses.
```math
\Gamma = \dfrac{-2 Z_{in} \pm \sqrt{4 Z_{in}^2 - 4Z_{in}^2 + 4Z_{out}^2}}{2(Z_{in} + Z_{out})}\qquad(16)
```

Reduce $`Z_{in}`$ in $`\sqrt{\quad}`$.
```math
\Gamma = \dfrac{-2 Z_{in} \pm \sqrt{4Z_{out}^2}}{2(Z_{in} + Z_{out})}\qquad(17)
```

And we can solve the square root easily now.
```math
\Gamma = \dfrac{-2 Z_{in} \pm 2Z_{out}}{2(Z_{in} + Z_{out})}\qquad(18)
```

Now we can reduce the fraction.
```math
\Gamma = \dfrac{-Z_{in} \pm Z_{out}}{Z_{in} + Z_{out}}\qquad(19)
```

And arrive into two solutions:
```math
\Gamma = \dfrac{-Z_{in} + Z_{out}}{Z_{in} + Z_{out}}\qquad \Gamma = \dfrac{-Z_{in} - Z_{out}}{Z_{in} + Z_{out}}\qquad(19)
```

The later solution is irrelevant (why???) as it always gives $`\Gamma = -1`$.
```math
\Gamma = \dfrac{-Z_{in} - Z_{out}}{Z_{in} + Z_{out}} = -\dfrac{Z_{in} + Z_{out}}{Z_{in} + Z_{out}} = -1 \qquad(20)
```

Thus we arrive into the only solution, later notation is the commonly used form.
```math
\Gamma = \dfrac{-Z_{in} + Z_{out}}{Z_{in} + Z_{out}} = \dfrac{Z_{out} - Z_{in}}{Z_{out} + Z_{in}}\qquad(21)
```
