# Euler equation - derivation

### Final formula
```math
e^{ix} = cos(x) + isin(x)
```

### Used derivations and derivation rules for combined functions
```math
\dfrac{d}{dx}e^{ax} = ae^x \newline { }\newline 
\dfrac{d}{dx}\sin(x) = \cos(x) \newline { }\newline
\dfrac{d}{dx}\cos(x) = -\sin(x) \newline { }\newline
\dfrac{d}{dx}(ab) = \dfrac{da}{dx}b+a\dfrac{db}{dx} \newline { }\newline
\dfrac{d}{dx}(a+b) = \dfrac{da}{dx}+\dfrac{db}{dx} \newline { }\newline
\dfrac{dy}{dx} = \dfrac{dy}{du}\dfrac{du}{dx}
```

### Premise
all complex numbers can be described in polar coordinates
```math
e^{ix} = r(cos(\theta) + isin(\theta))
```

Do not forget that:
```math
i^2 = -1
```

### Derivation

```math
e^{ix} = r(\cos(\theta) + i\sin(\theta))\qquad(1)
```

lets derive the whole equation along x.
```math
ie^{ix} = \dfrac{dr}{dx} (\cos(\theta) + i \sin(\theta)) + r \dfrac{d(\cos(\theta)+i \sin(\theta))}{dx}\qquad(2)
```

we can obtainin (3) by multiplying eq (1) by $`i`$. Then we obtain (4) by substituting (3) instead of $`ie^{ix}`$ on the left site of (2). 
```math
ie^{ix} = ir(\cos(\theta) + i \sin(\theta))\qquad(3) \newline { }\newline
ir(cos(\theta)+i\sin(\theta)) = \dfrac{dr}{dx}(\cos(\theta) + i\sin(\theta)) + r\dfrac{d(\cos(\theta)+i\sin(\theta))}{dx}\qquad(4)
```
Reducing the number of brackets we obtain (5) and subsequently (6):
```math
ir\cos(\theta)+i^2r\sin(\theta) = \dfrac{dr}{dx}\cos(\theta) + i\dfrac{dr}{dx}\sin(\theta) + r\dfrac{d(\cos(\theta))}{dx}+r\dfrac{d(i\sin(\theta))}{dx}\qquad(5) \newline { }\newline
ircos(\theta)-r\sin(\theta) = \dfrac{dr}{dx}\cos(\theta) + i\dfrac{dr}{dx}\sin(\theta) - r\sin(\theta)\dfrac{d\theta}{dx}+ir\cos(\theta)\dfrac{d\theta}{dx}\qquad(6)
```
Now we can separate real and imaginary part of the equation (6):
```math
-r\sin(\theta) = \dfrac{dr}{dx}\cos(\theta) -r\sin(\theta)\dfrac{d\theta}{dx}\qquad(7) \newline { }\newline
ir\cos(\theta) = + i\dfrac{dr}{dx}\sin(\theta) + ir\cos(\theta)\dfrac{d\theta}{dx}\qquad(8)
```
From both equations we see that we need $`\dfrac{dr}{dx} = 0`$ and $`\dfrac{d\theta}{dx} = 1`$ to get rid of the unwanted sine for the imaginary part and the cosine for the real part.
From $`e^{i0} = 1`$ we get to $`r = 1`$ and $`\theta = x`$.